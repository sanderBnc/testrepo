import { Selector, ClientFunction } from 'testcafe'; // first import testcafe selectors
import { AureliaSelector } from "testcafe-aurelia-selectors";

const getWindowLocation = ClientFunction(() => window.location);


fixture `ProductBoxLandingspagina` 
.page `https://test-winkel.bnc.nl/#/`;

test('product-box-component labels', async t => {    
    const productBoxesGreenStamp = Selector('.product__promotionStamp--green');
    const productBoxesRedStamp = Selector('.product__promotionStamp--red');
    const productBoxesINTHABUILDING = Selector('.product__promotionStampOrganization');
    await t
        .expect(productBoxesGreenStamp.exists).ok()
        .expect(productBoxesGreenStamp.innerText).eql('Aanbevolen')

        .expect(productBoxesRedStamp.exists).ok()
        .expect(productBoxesRedStamp.innerText).contains('%') //TODO is alleen %?;

        .expect(productBoxesINTHABUILDING.find('i').exists).ok()
        .expect(productBoxesINTHABUILDING.find('i').hasClass('fa-building')).ok();
});

test('product-box-component titel', async t => {    
    const pTitel = Selector('.pbcTitle');
    
    await t
        .expect(pTitel.exists).ok()
        .expect(pTitel.innerText).notEql('')
        .click(pTitel);
    
    const location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/)\w+(\/)\d+/g);
});

test('product-box-component image', async t => {    
    const imageDiv = Selector('.product__img');
    await t
        .expect(imageDiv.exists).ok()
        .expect(imageDiv.find('img').exists).ok()
        .expect(imageDiv.find('img').getAttribute('src')).notEql('')
        .click(imageDiv);
    
    const location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/)\w+(\/)\d+/g);
});

//TODO blauwe blokje testen, testcafe mist ::before selector
test('product-box-component selling points', async t => {    
    const divKsp = await Selector('.product__ksp');
    const divKspCount = await divKsp.childElementCount;

    await t.expect(divKspCount).eql(3);
    for(let i = 0; i < divKspCount; i++){
        await t
            .expect(divKsp.child(i).exists).ok()
            .expect(divKsp.child(i).find('p').exists).ok()
            .expect(divKsp.child(i).innerText).notEql('');
        
    }
});

test('product-box-component prijs', async t => {    
    const divPrice = Selector('.product__price');
    await t
        .expect(divPrice.exists).ok()
        .expect(divPrice).notEql('')
    const salePrice = Selector('.product__promotionStamp--red');
    const productBoxPrice = salePrice.parent().find('.product__price--lineThrough');
    const productBoxSalePrice = salePrice.parent().find('.product__price--red');
    await t
    .expect(productBoxPrice.exists).ok()
    .expect(productBoxSalePrice.exists).ok()
    .expect(productBoxSalePrice.getStyleProperty('color')).eql('rgb(' + (13*16+11) + ', ' + (5*16+2) + ', ' + (3*16 + 13) + ')')    //#db523d
    .expect(productBoxPrice.getStyleProperty('text-decoration-line')).eql('line-through');
});
















fixture `ProductBoxProductdetailPagina`// declare the fixture
.page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/555`;


test('product-box-component titel', async t => {    
    const pTitel = Selector('.pbcTitle');
    
    await t
        .expect(pTitel.exists).ok()
        .expect(pTitel.innerText).notEql('')
        .click(pTitel);
    
    const location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/)\w+(\/)\d+/g);
});

//TODO blauwe blokje testen
test('product-box-component selling points', async t => {    
    const divKsp = await Selector('.product__ksp');
    const divKspCount = await divKsp.childElementCount;
    
    await t.expect(divKspCount).eql(5);
    for(let i = 0; i < divKspCount; i++){
        await t
            .expect(divKsp.child(i).exists).ok()
            .expect(divKsp.child(i).innerText).notEql('');
    }
});

test('product-box-component prijs', async t => {    
    const divPrice = Selector('.product__price');
    await t
        .expect(divPrice.exists).ok()
        .expect(divPrice).notEql('')
    const salePrice = Selector('.product__promotionStamp--red');
    const productBoxPrice = salePrice.parent().find('.product__price--lineThrough');
    const productBoxSalePrice = salePrice.parent().find('.product__price--red');
    await t
    .expect(productBoxPrice.exists).ok()
    .expect(productBoxSalePrice.exists).ok()
    .expect(productBoxSalePrice.getStyleProperty('color')).eql('rgb(' + (13*16+11) + ', ' + (5*16+2) + ', ' + (3*16 + 13) + ')')    //#db523d
    .expect(productBoxPrice.getStyleProperty('text-decoration-line')).eql('line-through');
});


//TODO add x days/ not deliverable
test('product-box-component levertijd', async t => {    
    const deliveryTime = Selector('.product__delivery'); 
    await t
        .expect(deliveryTime.exists).ok()
        .expect(deliveryTime.innerText).notEql('')
});

test('product-box-component vergelijk', async t => {    
    const compareBox = Selector('.product__compare > div > p > .bncLabel'); 
    await t
        .expect(compareBox.exists).ok()
        .expect(compareBox.innerText).eql('Vergelijk')
        .click(compareBox);
    const compareFooter = Selector('.productsComparator__container');
    const pTitel = await Selector('.pbcTitle').innerText;
    await t
        .expect(compareFooter.exists).ok()
        .expect(compareFooter.innerText).contains(pTitel)
        .click(compareBox)
        .expect(compareFooter.exists).notOk();
});;

test('product-box-component input field', async t => {    
    const productInputField = Selector('.product__inputWrapper');
    await t
        .expect(productInputField.exists).ok()
        .click(productInputField);
});

test('product-box-component +-button', async t => {    
    const addButton = Selector('.product__plusButton');
    await t
        .expect(addButton.exists).ok()
        .click(addButton);
});

test('product-box-component hartje-button', async t => {    
    const favoriteButton = Selector('.product__heartButton');
    await t
        .expect(favoriteButton.exists).ok()
        .click(favoriteButton);
});















fixture `ProductBoxProductoverzichtPagina`// declare the fixture
.page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops`;

test('product-box-component labels', async t => {    
    const productBoxesGreenStamp = Selector('.product__promotionStamp--green');
    const productBoxesRedStamp = Selector('.product__promotionStamp--red');
    const productBoxesINTHABUILDING = Selector('.product__promotionStampOrganization');
    await t
        .click(Selector("label").withAttribute("for", "filterValue0-0"))
        .expect(productBoxesGreenStamp.exists).ok()
        .expect(productBoxesGreenStamp.textContent).contains('Aanbevolen')
        .click(Selector("label").withAttribute("for", "filterValue0-0"))

        .click(Selector("label").withAttribute("for", "filterValue0-1"))
        .expect(productBoxesRedStamp.exists).ok()
        .expect(productBoxesRedStamp.textContent).contains('%') //TODO is alleen %?;
        .click(Selector("label").withAttribute("for", "filterValue0-1"))

        .click(Selector("label").withAttribute("for", "filterValue0-2"))
        .expect(productBoxesINTHABUILDING.find('i').exists).ok()
        .expect(productBoxesINTHABUILDING.find('i').hasClass('fa-building')).ok();
});

test('product-box-component titel', async t => {    
    const pTitel = Selector('.pbcTitle');
    
    await t
        .expect(pTitel.exists).ok()
        .expect(pTitel.innerText).notEql('')
        .click(pTitel);
    
    const location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/)\w+(\/)\d+/g);
});

test('product-box-component image', async t => {    
    const imageDiv = Selector('.product__img');
    await t
        .expect(imageDiv.exists).ok()
        .expect(imageDiv.find('img').exists).ok()
        .expect(imageDiv.find('img').getAttribute('src')).notEql('')
        .click(imageDiv);
    
    const location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cat\/werkplekken\/)\w+(\/)\d+/g);
});


test('product-box-component product code', async t => {    
    const pTitel = Selector('.pbcTitle');
    await t
        .expect(pTitel.exists).ok()
        .expect(pTitel.innerText).notEql('');
});

test('product-box-component prijs', async t => {    
    const divPrice = Selector('.product__price');
    await t
        .expect(divPrice.exists).ok()
        .expect(divPrice).notEql('')
    const salePrice = Selector('.product__promotionStamp--red');
    const productBoxPrice = salePrice.parent().find('.product__price--lineThrough');
    const productBoxSalePrice = salePrice.parent().find('.product__price--red');
    await t
    .expect(productBoxPrice.exists).ok()
    .expect(productBoxSalePrice.exists).ok()
    .expect(productBoxSalePrice.getStyleProperty('color')).eql('rgb(' + (13*16+11) + ', ' + (5*16+2) + ', ' + (3*16 + 13) + ')')    //#db523d
    .expect(productBoxPrice.getStyleProperty('text-decoration-line')).eql('line-through');
});

//TODO blauwe blokje testen
test('product-box-component selling points', async t => {    
    const divKsp = Selector('.product__ksp');
    const divKspCount = await divKsp.childElementCount;
    await t.expect(divKspCount).eql(3);
    for(let i = 0; i < divKspCount; i++){
        await t
            .expect(divKsp.child(i).exists).ok()
            .expect(divKsp.child(i).innerText).notEql('');
    }
});

//TODO add x days/ not deliverable
test('product-box-component levertijd', async t => {    
    const deliveryTime = Selector('.product__delivery'); 
    await t
        .expect(deliveryTime.exists).ok()
        .expect(deliveryTime.innerText).notEql('')
});

test('product-box-component warning boxes', async t => {
    const productBox = Selector('.product');
    await t
        .typeText(productBox.find('.product__quantityInput'), "35", {replace: true});
    const warning = Selector('.product__delivery--warning');
    await t
        .expect(warning.exists).ok()
        .typeText(productBox.find('.product__quantityInput'), "5", {replace: true})
        .expect(warning.exists).notOk();
}); 

test('product-box-component vergelijk', async t => {    
    const compareBox = Selector('.product__compare > div > p > .bncLabel'); 
    await t
        .expect(compareBox.exists).ok()
        .expect(compareBox.innerText).eql('Vergelijk')
        .click(compareBox);
    const compareFooter = Selector('.productsComparator__container');
    const pTitel = await Selector('.pbcTitle').innerText;
    await t
        .expect(compareFooter.exists).ok()
        .expect(compareFooter.innerText).contains(pTitel)
        .click(compareBox)
        .expect(compareFooter.exists).notOk();
});

//	aantal invulveld: bij overschrijden van de voorraad verschijnt een 'warning' icoontje bij de levertijd. 
//  Hoveren over dat icoontje toont een melding met voor welke voorraad de levertijd geldt.
//	+button:
//	plaatje van product vliegt naar winkelmand.
//	product staat in de winkelmand popup en in de winkelmand pagina zelf met het juiste aantal uit het invulveld
//	voorraad is aangepast (bij verhogen van invulveld is de nieuwe voorraad weer te zien in de hover van het ‘warning’ icoon)

//TODO check animation?
test('product-box-component input field/add button', async t => {    
    //const divProductInputField = Selector('.product__inputWrapper');
    //const inputField = Selector('.product__inputWrapper > .product__quantityInput');
    const addButton = Selector('.product__plusButton');
    const shoppingCartButton = Selector('.shoppingBasket');
    const itemTitle = Selector('.pbcTitle');
    /*
    await t
        //testTODO
        //.typeText(AureliaSelector.byClickDelegate('checkQuantityParent', divProductInputField), '6')

        //check +-1 buttons  TODO 
        .expect(inputField.value).eql('1')
 
        //TODO help :/ up goes left...
        .typeText(inputField, "2", {replace: true})
        .expect(inputField.value).eql('2')
        //.pressKey('backspace')
        //.pressKey('up')
        //.pressKey('down')

        .typeText(inputField, "1", {replace: true})
        .expect(inputField.value).eql('1')
    */
    // TODO CHECK NUMBER OF WARNING?
    //check adding 1 item
    await t
        .click(shoppingCartButton)
        .expect(Selector('.orderLineWrapper').innerText).notContains(itemTitle.innerText)
        .click(addButton)
        .expect(Selector('.swing').exists).notOk()
        .expect(Selector('.swing').exists).ok()
        .click(shoppingCartButton);

    var itemQuantity = await Selector('.orderLine').withText(await itemTitle.innerText).find('input');
    await t
        .expect(Selector('.orderLineWrapper').innerText).contains(await itemTitle.innerText)
        .expect(itemQuantity.value).eql('1');
        

    let num = parseInt(await itemQuantity.value) + 1;
    var itemQuantity2 = await Selector('.orderLine').withText(await itemTitle.innerText).find('input');

    await t
        .click(addButton)
        .expect(Selector('.swing').exists).notOk()
        .expect(Selector('.swing').exists).ok()
        .click(shoppingCartButton)
        .expect(itemQuantity2.value).eql(num.toString());

    //check add 5
    const productBox = Selector('.product').withText(await itemTitle.innerText);
    await t
        .typeText(productBox.find('.product__quantityInput'), "5", {replace: true})
        .click(addButton)
        .expect(Selector('.swing').exists).notOk()
        .expect(Selector('.swing').exists).ok()
        .click(shoppingCartButton);


    num = num + 5;
    let itemQuantity3 = await Selector('.orderLine').withText(await itemTitle.innerText).find('input');
    await t
        .expect(itemQuantity3.value).eql(num.toString());
});

test('product-box-component hartje-button', async t => {    
    const favoriteButton = Selector('.product__heartButton');
    const favoriteButtonHeart = favoriteButton.find('i');
    
    await t
        .expect(favoriteButton.exists).ok()
        .expect(favoriteButtonHeart.exists).ok()
        .expect(favoriteButtonHeart.getAttribute('class')).contains('product__favorited')
        .expect(favoriteButtonHeart.getStyleProperty('color')).eql('rgb(' + (15*16+15) + ', ' + (11*16+4) + ', ' + (13*16+8) +')') // #0xXYZ => rbg(X, Y, Z) 
        .click(favoriteButton)
        .expect(favoriteButtonHeart.getStyleProperty('color')).eql('rgb(255, 255, 255)');
    //TODO	product is toegevoegd aan wishlist pagina (werkt nog niet)
});


/*  TODO
Uitzondering
In de secties ‘accessoires’, 
‘alternatieven’ (op productdetail pagina) en 
‘eerder bekeken producten’ (op alle pagina’s) komen ook productboxes voor. 
Die hebben dezelfde layout als op de landingspagina. 
Verschil is dat niet alleen het plaatje en de titel een link zijn, maar de complete productbox. 
De link leidt niet naar de productdetailpagina, maar opent een popup met specs van dat product.
*/
