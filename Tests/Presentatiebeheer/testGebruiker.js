import { Selector } from 'testcafe'; // first import testcafe selectors
import { ClientFunction } from 'testcafe';


fixture `Getting Started`// declare the fixture
    .page `https://test.presentatiebeheer.nl/login`;  // specify the start page


//login suite
test('Login without username and password', async t => {
    await t
        .click(Selector('.bncSignIn'))
        .expect(Selector('.bncFeedbackMessage').innerText).eql('Ongeldige gebruikersnaam of wachtwoord');
});

test('Login with username, without password', async t => {
    await t
        .click(Selector('#username'))
        .typeText(Selector('#username'), 'sanderh')
        .click(Selector('.bncSignIn'))

        .expect(Selector('.bncFeedbackMessage').innerText).eql('Ongeldige gebruikersnaam of wachtwoord');
});

test('Login with username and wrong password', async t => {
    await t
        .click(Selector('#username'))
        .typeText(Selector('#username'), 'sanderh')

        .click(Selector('#identifier'))
        .typeText(Selector('#identifier'), 'Test123')
        
        .click(Selector('.bncSignIn'))

        .expect(Selector('.bncFeedbackMessage').innerText).eql('Ongeldige gebruikersnaam of wachtwoord');
});


test('Login with username and right password', async t => {
    const getLocation = ClientFunction(() => document.location.href.toString());
    await t
        .click(Selector('#username'))
        .typeText(Selector('#username'), 'sander.vanderhaak@bnc.nl')

        .click(Selector('#identifier'))
        .typeText(Selector('#identifier'), 'Test1234')
        
        .click(Selector('.bncSignIn'))


        .expect(getLocation()).match(/https:\/\/test.presentatiebeheer.nl\/slides\/channels/);
});


test('Wachtwoord vergeten', async t => {
    await t
        .click(Selector('a').withAttribute("ng-click", "controller.goForgotPassword()"))

        .expect(Selector('div').withAttribute("ng-if", "controller.email == ''").exists).ok();
});
