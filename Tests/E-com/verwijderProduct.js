import { Selector, ClientFunction } from 'testcafe';

const getWindowLocation = ClientFunction(() => window.location);

fixture `ProductBoxdetailpagina` 
.page `https://test-winkel.bnc.nl/#/cat/werkplekken/Laptops/545`;


test('Voeg toe en verwijder item winkelwagenPopup', async t => {
    const location = await getWindowLocation();
    const addButton = await Selector('.product__plusButton');
    const shoppingCartButton = Selector('.shoppingBasket');
    const itemTitle = await Selector('.pbcTitle').innerText;

    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/)/)
        .click(addButton)
        .click(shoppingCartButton)
        .expect(Selector('.orderLineWrapper').innerText).contains(itemTitle);

    const itemDivGarbageCan = await Selector('.orderLine').withText(itemTitle).find('i');
    await t
        .click(itemDivGarbageCan)
        .expect(Selector('.orderLineWrapper').innerText).notContains(itemTitle);  
});

test('Voeg toe en verwijder item winkelwagenPagina', async t => {
    let location = await getWindowLocation();
    const addButton = await Selector('.product__plusButton');
    const shoppingCartButton = Selector('.shoppingBasket');
    const itemTitle = await Selector('.pbcTitle').innerText;

    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/)/)
        .click(addButton)
        .click(shoppingCartButton)
        .click('.icons__link-to-cart-container')

    location = await getWindowLocation();
    await t
        .expect(location.href).eql('https://test-winkel.bnc.nl/#/cart')
        .expect(Selector('.cart').innerText).contains(itemTitle);

    const itemDivGarbageCan = await Selector('compose').withText(itemTitle).find('i').withAttribute('click.delegate', 'deleteItem(item)');
    await t
        .click(itemDivGarbageCan)
        .expect(Selector('.cart').innerText).notContains(itemTitle);  
});
