import { Selector, ClientFunction } from 'testcafe'; // first import testcafe selectors

const getWindowLocation = ClientFunction(() => window.location);

fixture `ProductBoxDetailpagina` 
.page `https://test-winkel.bnc.nl/#/cart`;

test('Verstuur winkelmandje', async t => {
    let location = await getWindowLocation();
    await t
        .expect(location.href).match(/(https:\/\/test-winkel.bnc.nl\/#\/cart)/);
    
    const sendCart = Selector('button').withAttribute('click.delegate', 'sendCart()');
    await t
        .click(sendCart)
        .expect(Selector('.customErrorButton').withExactText('Je hebt foutmeldingen!').exists).ok()
        .typeText(Selector('input').withAttribute('value.bind', 'inputData.referenceNumber & validate'), '11010010101101010100101')
        .click(sendCart)
        .expect(Selector('.ecomDialog').exists).ok();
        //TODO check annuleren/order versturen als het af is
});
